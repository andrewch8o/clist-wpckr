The project solves the problem of ongoing monitoring for new ADs and sending email notifications for those ADs that meet criteria.
Consists of two components:

* Tampermonkey extension (javascript) in '/tmclient' to interact with "map view" API and submit data to the web server
* Django web server (python) in '/wpcksrv' receiving the data from the browser extension, saving new ads to the database and sending email notifications