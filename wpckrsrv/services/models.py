from django.db import models

# Create your models here.
class Post(models.Model):
    id = models.CharField(max_length=40, primary_key=True)
    url = models.CharField(max_length=300)
    zip = models.CharField(max_length=10)
    imgs_json = models.TextField()