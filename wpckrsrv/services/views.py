from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.utils import IntegrityError
from urllib.parse import urlparse, parse_qs
from services.models import Post
import json
import logging

logger = logging.getLogger(__name__)

@csrf_exempt
def index(request):
    rdata = json.loads(request.body)
    search_url = rdata['org']
    image_list = rdata['img']
    listing_url = rdata['url'][0]
    
    search_parsed = urlparse(search_url)
    search_qs = search_parsed.query
    search_params = parse_qs(search_qs)
    search_zip = search_params['postal']

    listing_parsed = urlparse(listing_url)
    post_id = listing_parsed.path.split('/')[-1]

    post = Post(
        id = post_id,
        url = listing_url,
        zip = search_zip,
        imgs_json = json.dumps(image_list)
    )

    #todo: catch post exists
    try:
        post.save(force_insert=True)
        logger.info("CREATED post '{}'".format(post.id))
    except IntegrityError:
        logger.info("IGNORED post '{}' already exists".format(post.id))
    return HttpResponse('OK')