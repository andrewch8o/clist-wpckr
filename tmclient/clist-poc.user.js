// ==UserScript==
// @name         clist-poc
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://sfbay.craigslist.org/search/apa*
// @grant        GM_xmlhttpRequest
// ==/UserScript==

(function() {
    'use strict';

    function getRandomInt(min, max) {
       min = Math.ceil(min);
       max = Math.floor(max);
       return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    // Converts an image to base64 URL
    function toDataURL(src, callback, outputFormat) {
        var img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function() {
            var canvas = document.createElement('CANVAS');
            var ctx = canvas.getContext('2d');
            var dataURL;
            canvas.height = this.naturalHeight;
            canvas.width = this.naturalWidth;
            ctx.drawImage(this, 0, 0);
            dataURL = canvas.toDataURL(outputFormat);
            callback(dataURL);
        };
        img.src = src;
        if (img.complete || img.complete === undefined) {
            img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
            img.src = src;
        }
    }

    function submitImageData(postData) {
            GM_xmlhttpRequest ( {
                method:     "POST",
                url:        "http://127.0.0.1:8000/services/imgdata",
                data:       JSON.stringify(postData),
                headers:    {
                    "Content-Type": "application/json"
                },
                onload:     function (response) {
                    if (response.status == 200 ) {
                        console.log ("[OK] IMG service endpoint");
                    } else {
                        console.warn("/!\ [NOT OK] IMG service endpoint");
                    }
                }
            } );
    }

    //RELOADING PAGE
    var restartInMin = getRandomInt(15,22);
    setTimeout(function(){
        console.log('Reloading page');
        location.reload();
    //TODO: random reload ~15-20 minutes
    }, restartInMin*60*1000);
    console.log("Set to refresh in '"+restartInMin+"' min");

    /*READING FIRST ITEM*/
    // ex_base - base ( ainticipated ) execution time of async image load after slider click
    function process_record(record, jQ, click_delay, click_flux, ex_base, index) {
        var jRecord = jQ(record);
        var jSlider = jRecord.find(".slider-forward.arrow").eq(0);

        var click_timeout = (index * click_delay) + click_flux - getRandomInt(0, 2*click_flux);
        console.debug('Setting click in '+ click_timeout +' ms');
        setTimeout(function() {
            jSlider.trigger("mousedown");
        }, click_timeout);

        var processing_timeout = ( click_delay + click_flux + ex_base ) * (index + 1);
        console.debug('Setting processing in '+ processing_timeout +' ms');
        var url = jRecord.find("a.result-image.gallery").map(function() { return jQ(this).attr('href'); }).get();
        setTimeout(function() {
            var postData = { 'url': url, 'org': window.location.href };
            GM_xmlhttpRequest ( {
                method:     "POST",
                url:        "http://127.0.0.1:8000/services/",
                data:       JSON.stringify(postData),
                headers:    {
                    "Content-Type": "application/json"
                },
                onload:     function (response) {
                    if (response.status == 200 ) {
                        console.log ("[OK] Service endpoint");
                    } else {
                        console.warn("/!\ [NOT OK] Service endpoint");
                    }
                }
            } );
            // submitting images after data has been sent
            setTimeout(function() {
                        var imgs = jRecord.find(".swipe-wrap img")
                            .each(function(){
                               return toDataURL(jQ(this).attr('src'),
                                                function(data) { submitImageData({data: data, url: url}) },
                                                'png');});
                        }, 3000);
        }, processing_timeout);
    }

    // Clicking the link
    //$(".result-row").eq(1).find(".slider-forward.arrow").eq(0).trigger("mousedown")

    function process_page(jQ) {
        jQ(".result-row").each( function(index, element){ process_record(this, jQ, 2000, 750, 2000, index); });
    }
    $(document).ready(process_page);
})();